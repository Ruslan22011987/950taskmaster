﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XUnitTestHotelServis.Migrations
{
    public partial class HotelServis : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AddDBEntrys",
                table: "AddDBEntrys");

            migrationBuilder.RenameTable(
                name: "AddDBEntrys",
                newName: "HotelServis");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HotelServis",
                table: "HotelServis",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_HotelServis",
                table: "HotelServis");

            migrationBuilder.RenameTable(
                name: "HotelServis",
                newName: "AddDBEntrys");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AddDBEntrys",
                table: "AddDBEntrys",
                column: "id");
        }
    }
}
